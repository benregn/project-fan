from follow import models

import logging
logger = logging.getLogger('project')
def indicate_already_following(profile, people):
    people_tmdb_ids = [
        person['id']
        for person in people
    ]
    already_following = set(
        models.FollowPeople.objects
            .filter(person__in=people_tmdb_ids)
            .values_list('person', flat=True)
    )
    logger.info(already_following)
    for person in people:
        person['is_following'] = person['id'] in already_following
    return people
