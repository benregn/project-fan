import requests
import requests_cache

from django.conf import settings

if settings.REQUESTS_CACHE_ENABLED:
    requests_cache.install_cache('tmdb')


def search_people(query):
    results = requests.get(
        'http://api.themoviedb.org/3/search/person',
        params={
            'api_key': settings.TMDB_API_KEY,
            'query': query,
        }
    )

    return structure_results(results.json()['results'])


def structure_results(results):
    return [
        {
            'id': result['id'],
            'name': result['name'],
            'profile_path': f"http://image.tmdb.org/t/p/w45{result['profile_path']}" if result['profile_path'] else None,
            'known_for': [
                each.get('title') or each.get('name')
                for each in result['known_for']
            ],
        }
        for result in results
    ]
