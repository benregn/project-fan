from django.views import generic

from . import service
from . import tmdb


class Search(generic.TemplateView):
    template_name = 'search/search.html'
    http_method_names = ['get']

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        query = self.request.GET.get('query')
        user = self.request.user

        if not query:
            return data

        results = tmdb.search_people(query)
        if not user.is_anonymous:
            results = service.indicate_already_following(user.profile, results)
        data['results'] = results

        return data
