from django.urls import path
from . import views
from . import apps

# app_name = 'follow'
app_name = apps.FollowConfig.name
urlpatterns = [
    path('follow/person', views.FollowPerson.as_view(), name='follow_person'),
    path('follow/feed', views.FollowFeedList.as_view(), name='follow_feed'),
    path('follow/', views.FollowPeopleList.as_view(), name='follow_list'),
]
