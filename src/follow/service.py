import django.db

from . import models
from . import tasks

import logging
logger = logging.getLogger('project')

def follow_person(user, person_name, person_tmdb_id, person_profile_path):
    logger.info(f'person id: {person_tmdb_id}')
    person, created = models.People.objects.get_or_create(
        id=person_tmdb_id,
        name=person_name,
        profile_path=person_profile_path,
    )
    created = True
    if created:
        logger.info('event=call_populate_person')
        tasks.populate_person.delay(person_tmdb_id)

    try:
        models.FollowPeople.objects.create(profile=user.profile, person=person)
    except django.db.IntegrityError:
        pass
