from django.contrib import admin

from . import models

class PeopleAdmin(admin.ModelAdmin):
    pass


class MediaAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'type')


class CreditsAdmin(admin.ModelAdmin):
    list_display = ('person', 'media', 'role', 'type')


admin.site.register(models.People, PeopleAdmin)
admin.site.register(models.Media, MediaAdmin)
admin.site.register(models.Credits, CreditsAdmin)
