import itertools
import logging

from celery import shared_task
from psqlextra.query import ConflictAction

from . import models
from . import tmdb

logger = logging.getLogger('project')


@shared_task
def populate_person(tmdb_id):
    credits = tmdb.get_person_credits(tmdb_id)
    media_ids = set()
    media_rows = []
    credits_rows = []

    logger.info(f'event=populate_person_prepare tmdb_id={tmdb_id} cast={len(credits["cast"])} cast={len(credits["crew"])}')

    for credit in itertools.chain(credits['cast'], credits['crew']):
        if not credit.get('release_date') and not credit.get('first_air_date'):
            logger.warn(f'event=no_date id={credit["id"]}')
            continue

        if credit['id'] not in media_ids:
            media_rows.append(dict(
                id=credit['id'],
                title=credit.get('title') or credit.get('name'),
                type=credit['media_type'],
                poster_path=credit['poster_path'],
                release_date=credit.get('release_date') or credit.get('first_air_date'),
            ))
            media_ids.add(credit['id'])

        if credit.get('character'):
            credits_rows.append(dict(
                person_id=tmdb_id,
                media_id=credit['id'],
                role=credit['character'],
                type='cast',
            ))

        if credit.get('job'):
            credits_rows.append(dict(
                person_id=tmdb_id,
                media_id=credit['id'],
                role=credit['job'],
                type='crew',
            ))

    if media_rows:
        _ = (models.Media.psqlextra
            .on_conflict(
                fields=['id'],
                action=ConflictAction.NOTHING,
            )
            .bulk_insert(media_rows))
    if credits_rows:
        _ = (models.Credits.psqlextra
            .on_conflict(
                fields=['person_id', 'media_id', 'role', 'type'],
                action=ConflictAction.NOTHING,
            )
            .bulk_insert(credits_rows))
    logger.info('event=populate_person_done')
