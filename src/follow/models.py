import uuid

from django.db import models
from django.conf import settings

from psqlextra.manager import PostgresManager
from psqlextra.indexes import ConditionalUniqueIndex


# class BaseProfile(models.Model):
#     user = models.OneToOneField(settings.AUTH_USER_MODEL,
#                                 on_delete=models.CASCADE,
#                                 primary_key=True)
#     slug = models.UUIDField(default=uuid.uuid4, blank=True, editable=False)

class People(models.Model):
    """Model definition for People."""

    id = models.IntegerField('The TMDB ID of this person', primary_key=True)
    name = models.TextField()
    profile_path = models.TextField(null=True)
    followers = models.ManyToManyField(
        'profiles.Profile',
        through='FollowPeople',
        related_name='following',
    )

    psqlextra = PostgresManager()
    objects = models.Manager()

    class Meta:
        """Meta definition for People."""

        verbose_name = 'People'
        verbose_name_plural = 'People'

    def __str__(self):
        """Unicode representation of People."""
        return f'{self.name} [tmdb_id={self.id}]'

    def get_absolute_url(self):
        """Return absolute url for People."""
        return ('')

    # TODO: Define custom methods here


class Media(models.Model):
    """Model definition for Media."""

    id = models.IntegerField('The TMDB ID of this media', primary_key=True)
    title = models.TextField()
    type = models.TextField()
    poster_path = models.TextField(null=True)
    release_date = models.DateField()

    people = models.ManyToManyField(
        'People',
        through='credits',
        related_name='media',
        blank=True,
    )

    psqlextra = PostgresManager()
    objects = models.Manager()

    class Meta:
        """Meta definition for Media."""

        verbose_name = 'Media'
        verbose_name_plural = 'Media'

    def __str__(self):
        """Unicode representation of Media."""
        return f'{self.title} [tmdb_id={self.id}]'


class FollowPeople(models.Model):
    profile = models.ForeignKey(
        'profiles.Profile',
        on_delete=models.CASCADE,
        related_name='_following',
    )
    person = models.ForeignKey(
        People,
        on_delete=models.CASCADE,
        related_name='_followers',
    )
    followed_at = models.DateTimeField(auto_now_add=True)

    psqlextra = PostgresManager()
    objects = models.Manager()

    class Meta:
        unique_together = ('profile', 'person')
        indexes = [
            models.Index(fields=['profile', 'person']),
        ]

    def __str__(self):
        return f'{self.profile} is following {self.person}'


class Credits(models.Model):
    person = models.ForeignKey(
        People,
        on_delete=models.CASCADE,
        related_name='characters_jobs',
    )
    media = models.ForeignKey(
        Media,
        on_delete=models.CASCADE,
        related_name='cast_crew',
    )
    role = models.TextField(blank=True)
    type = models.TextField(blank=True)

    psqlextra = PostgresManager()
    objects = models.Manager()

    class Meta:
        verbose_name_plural = 'Credits'
        unique_together = ('person', 'media', 'role', 'type')

    def __str__(self):
        return f'{self.person} is in {self.media} as {self.role} ({self.type})'
