# Generated by Django 2.0.5 on 2018-05-09 07:46

from django.db import migrations, models
import django.db.models.deletion
import psqlextra.indexes.conditional_unique_index
import psqlextra.manager.manager


class Migration(migrations.Migration):

    replaces = [('follow', '0001_squashed_0004_auto_20180501_0922'), ('follow', '0002_auto_20180501_1950'), ('follow', '0003_media_release_date'), ('follow', '0004_auto_20180504_0730'), ('follow', '0005_auto_20180506_0933'), ('follow', '0006_auto_20180506_0940'), ('follow', '0007_auto_20180506_0941'), ('follow', '0008_auto_20180506_0946'), ('follow', '0009_auto_20180507_0756'), ('follow', '0010_auto_20180507_1342'), ('follow', '0011_auto_20180508_0835'), ('follow', '0012_auto_20180508_1337'), ('follow', '0013_auto_20180508_1412')]

    initial = True

    dependencies = [
        ('profiles', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='FollowPeople',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('followed_at', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Media',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('tmdb_id', models.IntegerField()),
                ('title', models.TextField()),
                ('type', models.TextField()),
                ('poster_path', models.TextField()),
            ],
            options={
                'verbose_name': 'Media',
                'verbose_name_plural': 'Media',
            },
        ),
        migrations.CreateModel(
            name='People',
            fields=[
                ('tmdb_id', models.IntegerField(primary_key=True, serialize=False)),
                ('name', models.TextField()),
                ('profile_path', models.TextField(null=True)),
                ('followers', models.ManyToManyField(related_name='following', through='follow.FollowPeople', to='follow.People')),
            ],
            options={
                'verbose_name': 'People',
                'verbose_name_plural': 'People',
            },
        ),
        migrations.AddField(
            model_name='followpeople',
            name='person',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='people', to='follow.People'),
        ),
        migrations.AddField(
            model_name='followpeople',
            name='profile',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='users', to='profiles.Profile'),
        ),
        migrations.AddIndex(
            model_name='followpeople',
            index=models.Index(fields=['profile', 'person'], name='follow_foll_profile_578c51_idx'),
        ),
        migrations.AlterUniqueTogether(
            name='followpeople',
            unique_together={('profile', 'person')},
        ),
        migrations.CreateModel(
            name='Credits',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('character', models.TextField(blank=True)),
                ('job', models.TextField(blank=True)),
                ('media', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='cast_crew', to='follow.Media')),
            ],
        ),
        migrations.AlterField(
            model_name='followpeople',
            name='person',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='_followers', to='follow.People'),
        ),
        migrations.AlterField(
            model_name='followpeople',
            name='profile',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='_following', to='profiles.Profile'),
        ),
        migrations.AlterField(
            model_name='people',
            name='followers',
            field=models.ManyToManyField(related_name='following', through='follow.FollowPeople', to='profiles.Profile'),
        ),
        migrations.AddField(
            model_name='credits',
            name='person',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='characters_jobs', to='follow.People'),
        ),
        migrations.AddField(
            model_name='media',
            name='people',
            field=models.ManyToManyField(blank=True, related_name='media', through='follow.Credits', to='follow.People'),
        ),
        migrations.AddIndex(
            model_name='credits',
            index=models.Index(fields=['person', 'media'], name='follow_cred_person__b4de8a_idx'),
        ),
        migrations.AlterUniqueTogether(
            name='credits',
            unique_together={('person', 'media')},
        ),
        migrations.AlterModelOptions(
            name='credits',
            options={'verbose_name_plural': 'Credits'},
        ),
        migrations.AddField(
            model_name='media',
            name='release_date',
            field=models.DateField(),
        ),
        migrations.RemoveField(
            model_name='media',
            name='id',
        ),
        migrations.AlterField(
            model_name='media',
            name='poster_path',
            field=models.TextField(null=True),
        ),
        migrations.AlterField(
            model_name='media',
            name='tmdb_id',
            field=models.IntegerField(primary_key=True, serialize=False),
        ),
        migrations.AlterModelManagers(
            name='credits',
            managers=[
                ('psqlextra', psqlextra.manager.manager.PostgresManager()),
            ],
        ),
        migrations.AlterModelManagers(
            name='followpeople',
            managers=[
                ('psqlextra', psqlextra.manager.manager.PostgresManager()),
            ],
        ),
        migrations.AlterModelManagers(
            name='media',
            managers=[
                ('objects', psqlextra.manager.manager.PostgresManager()),
            ],
        ),
        migrations.AlterModelManagers(
            name='people',
            managers=[
                ('objects', psqlextra.manager.manager.PostgresManager()),
            ],
        ),
        migrations.RemoveField(
            model_name='media',
            name='tmdb_id',
        ),
        migrations.RemoveField(
            model_name='people',
            name='tmdb_id',
        ),
        migrations.AddField(
            model_name='media',
            name='id',
            field=models.IntegerField(default=1, primary_key=True, serialize=False, verbose_name='The TMDB ID of this media'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='people',
            name='id',
            field=models.IntegerField(default=1, primary_key=True, serialize=False, verbose_name='The TMDB ID of this person'),
            preserve_default=False,
        ),
        migrations.AlterModelManagers(
            name='media',
            managers=[
                ('psqlextra', psqlextra.manager.manager.PostgresManager()),
            ],
        ),
        migrations.AlterModelManagers(
            name='people',
            managers=[
                ('psqlextra', psqlextra.manager.manager.PostgresManager()),
            ],
        ),
        migrations.RemoveIndex(
            model_name='credits',
            name='follow_cred_person__b4de8a_idx',
        ),
        migrations.AddIndex(
            model_name='credits',
            index=psqlextra.indexes.conditional_unique_index.ConditionalUniqueIndex(condition="character <> ''", fields=['person', 'media', 'character'], name='follow_cred_person__09ba20_idx'),
        ),
        migrations.AddIndex(
            model_name='credits',
            index=psqlextra.indexes.conditional_unique_index.ConditionalUniqueIndex(condition="job <> ''", fields=['person', 'media', 'job'], name='follow_cred_person__fbce92_idx'),
        ),
        migrations.RemoveIndex(
            model_name='credits',
            name='follow_cred_person__09ba20_idx',
        ),
        migrations.RemoveIndex(
            model_name='credits',
            name='follow_cred_person__fbce92_idx',
        ),
        migrations.AlterUniqueTogether(
            name='credits',
            unique_together=set(),
        ),
        migrations.AddIndex(
            model_name='credits',
            index=psqlextra.indexes.conditional_unique_index.ConditionalUniqueIndex(condition="character = ''", fields=['person', 'media', 'character'], name='follow_cred_person__09ba20_idx'),
        ),
        migrations.AddIndex(
            model_name='credits',
            index=psqlextra.indexes.conditional_unique_index.ConditionalUniqueIndex(condition="job = ''", fields=['person', 'media', 'job'], name='follow_cred_person__fbce92_idx'),
        ),
        migrations.CreateModel(
            name='Cast',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('character', models.TextField(blank=True)),
            ],
            options={
                'verbose_name_plural': 'Cast',
            },
            managers=[
                ('psqlextra', psqlextra.manager.manager.PostgresManager()),
            ],
        ),
        migrations.CreateModel(
            name='Crew',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('job', models.TextField(blank=True)),
            ],
            options={
                'verbose_name_plural': 'Crew',
            },
            managers=[
                ('psqlextra', psqlextra.manager.manager.PostgresManager()),
            ],
        ),
        migrations.RemoveField(
            model_name='credits',
            name='media',
        ),
        migrations.RemoveField(
            model_name='credits',
            name='person',
        ),
        migrations.RemoveField(
            model_name='media',
            name='people',
        ),
        migrations.DeleteModel(
            name='Credits',
        ),
        migrations.AddField(
            model_name='crew',
            name='media',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='crew_list', to='follow.Media'),
        ),
        migrations.AddField(
            model_name='crew',
            name='person',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='jobs', to='follow.People'),
        ),
        migrations.AddField(
            model_name='cast',
            name='media',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='cast_list', to='follow.Media'),
        ),
        migrations.AddField(
            model_name='cast',
            name='person',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='characters', to='follow.People'),
        ),
        migrations.AlterUniqueTogether(
            name='crew',
            unique_together=set(),
        ),
        migrations.AlterUniqueTogether(
            name='cast',
            unique_together=set(),
        ),
        migrations.CreateModel(
            name='Credits',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('role', models.TextField(blank=True)),
                ('type', models.TextField(blank=True)),
                ('media', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='cast_crew', to='follow.Media')),
                ('person', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='characters_jobs', to='follow.People')),
            ],
            options={
                'verbose_name_plural': 'Credits',
            },
            managers=[
                ('psqlextra', psqlextra.manager.manager.PostgresManager()),
            ],
        ),
        migrations.RemoveField(
            model_name='cast',
            name='media',
        ),
        migrations.RemoveField(
            model_name='cast',
            name='person',
        ),
        migrations.RemoveField(
            model_name='crew',
            name='media',
        ),
        migrations.RemoveField(
            model_name='crew',
            name='person',
        ),
        migrations.DeleteModel(
            name='Cast',
        ),
        migrations.DeleteModel(
            name='Crew',
        ),
        migrations.AddField(
            model_name='media',
            name='people',
            field=models.ManyToManyField(blank=True, related_name='media', through='follow.Credits', to='follow.People'),
        ),
        migrations.AlterUniqueTogether(
            name='credits',
            unique_together={('person', 'media', 'role', 'type')},
        ),
    ]
