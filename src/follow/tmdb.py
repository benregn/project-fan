import requests
import requests_cache

from django.conf import settings

if settings.REQUESTS_CACHE_ENABLED:
    requests_cache.install_cache('tmdb')


def get_person_credits(tmdb_id):
    results = requests.get(
        f'http://api.themoviedb.org/3/person/{tmdb_id}/combined_credits',
        params={
            'api_key': settings.TMDB_API_KEY,
        }
    )

    return results.json()
