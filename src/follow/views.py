import logging

from django.views import generic
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.postgres.aggregates import ArrayAgg

from . import models
from . import service

logger = logging.getLogger('project')


class FollowPerson(LoginRequiredMixin, generic.View):

    def post(self, request, *args, **kwargs):
        user = self.request.user
        name = request.POST['name']
        tmdb_id = request.POST['tmdb_id']
        profile_path = request.POST['profile_path']

        logger.info(f'{user} followed {name} [{tmdb_id}]')
        service.follow_person(
            user=user,
            person_name=name,
            person_tmdb_id=tmdb_id,
            person_profile_path=profile_path,
        )

        return HttpResponse(status=201)


class FollowPeopleList(LoginRequiredMixin, generic.ListView):

    template_name = 'follow/follow_people_list.html'
    context_object_name = 'people'

    def get_queryset(self):
        user = self.request.user
        queryset = (
            models.FollowPeople.objects
                .filter(profile=user.profile)
                .select_related('person')
        )
        return queryset


class FollowFeedList(generic.ListView):

    template_name = 'follow/follow_feed_list.html'

    def get_queryset(self):
        user = self.request.user
        # queryset = (
        #     models.FollowPeople.objects
        #         .filter(profile=user.profile)
        #         .prefetch_related('person__characters_jobs')
        # )
        # queryset = (
        #     models.People.objects
        #         .filter(followers=user.profile)
        #         # .select_related('characters', 'jobs')
        #         .prefetch_related('characters', 'jobs')
        # )
        # queryset = (
        #     models.Credits.objects
        #     .filter(person__followers=user.profile)
        #     .filter(media__release_date__gte='2018-01-01')
        #     # .select_related('person', 'media')
        #     .values('media__title', 'media__release_date')
        #     .annotate(credits=ArrayAgg('person__name'))
        # )
        # queryset = (
        #     models.Media.objects
        #     .filter(people__followers=user.profile)
        #     .values('title', 'release_date', 'poster_path')
        # )
        results = my_custom_sql(user.id)
        logger.info(results[0]['people'])
        logger.info(type(results[0]['people']))
        return results
        return queryset


from django.db import connection

def my_custom_sql(profile_id):
    with connection.cursor() as cursor:
        # array_agg((p.id, p.name, c.role, c.type)) as people
        query = """
SELECT
    m.title,
    m.release_date,
    json_agg(json_build_object('id', p.id, 'name', p.name, 'role', c.role, 'type', c.type)) as people,
    m.release_date < now() as in_past,
    m.poster_path
FROM follow_followpeople fp
INNER JOIN follow_credits c
    ON c.person_id = fp.person_id
INNER JOIN follow_people p
    ON p.id = fp.person_id
INNER JOIN follow_media m
    ON m.id = c.media_id
WHERE fp.profile_id = %s and m.release_date > '2018-01-01'
GROUP BY m.title, m.release_date, m.poster_path
ORDER BY m.release_date;
"""
        args = [
            profile_id
        ]
        cursor.execute(query, args)
        rows = dictfetchall(cursor)

    return rows


def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
